
import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/addSkMhs.dart';
import 'package:sikp/apiservices.dart';

class MenuSuratKp extends StatefulWidget {
  const MenuSuratKp({Key key}) : super(key: key);

  @override
  _MenuSuratKpState createState() => _MenuSuratKpState();
}

class _MenuSuratKpState extends State<MenuSuratKp> {
  List<skkp> listskkp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Surat Keteranagan KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>SuratKeterangan()));
              })
        ],
      ),

      body: FutureBuilder(
          future: apiservices().getSkKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<skkp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listskkp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listskkp[position].nim + " - " +
                            listskkp[position].lembaga + " , " + listskkp[position].lembaga),
                        subtitle: Text(
                            "no_telp : " + listskkp[position].no_telp + "\n" +
                                "dokumen : " + listskkp[position].dokumen
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listskkp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
