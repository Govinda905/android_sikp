
import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/apiservices.dart';

class MenuKp extends StatefulWidget {
  const MenuKp({Key key}) : super(key: key);

  @override
  _MenuKpState createState() => _MenuKpState();
}

class _MenuKpState extends State<MenuKp> {

  List<kp> listkp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: apiservices().getKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listkp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listkp[position].nim + " - " +
                              listkp[position].semester + " , " + listkp[position].tahun),
                          subtitle: Text(
                              "judul_kerja_praktik: " + listkp[position].judul_kerja_praktik + "\n" +
                                  "status_kp : " + listkp[position].status_kp
                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listkp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
