
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sikp/menuRegisSurat.dart';
import 'package:sikp/menu_regiskp.dart';
import 'package:sikp/menuregisPraKp.dart';

import 'main.dart';

class Koor extends StatefulWidget {
  final String nama, email, foto;

  Koor({this.nama, this.email, this.foto});
  @override
  _KoorState createState() => _KoorState();
}

class _KoorState extends State<Koor> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("HALAMAN KOOR"),
        backgroundColor: Colors.deepOrange[800],
      ),
      backgroundColor: Colors.deepOrange[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.timer, size: 60.0,
                        color: Colors.green,),
                      Text("Mengatur Batas KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.timer, size: 60.0,
                        color: Colors.green,),
                      Text("Mengatur Ujian KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegisPrakp()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 60.0, color: Colors.green,),
                      Text(
                          "Daftar Regis Pra Kp", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuKp()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 60.0, color: Colors.green,),
                      Text("Daftar Regis KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuRegisSurat()));},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 60.0, color: Colors.green,),
                      Text("Pengajuan Sk",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 60.0, color: Colors.green,),
                      Text("Daftar Bimbingan KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file_outlined, size: 60.0, color: Colors.green,),
                      Text("Ujian Kp",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
