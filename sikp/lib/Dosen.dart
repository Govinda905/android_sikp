
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'main.dart';

class Dosen extends StatefulWidget {
  final String nama, email, foto;

  Dosen({this.nama, this.email, this.foto});
  @override
  _DosenState createState() => _DosenState();
}

class _DosenState extends State<Dosen> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("HALAMAN DOSEN"),
        backgroundColor: Colors.deepOrange[800],
      ),
      backgroundColor: Colors.deepOrange[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.list_alt_sharp, size: 70.0,
                        color: Colors.purple,),
                      Text("Daftar Bimbingan KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.list_alt_sharp, size: 70.0,
                        color: Colors.blue,),
                      Text("Ujian KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
