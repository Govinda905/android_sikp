import 'package:http/http.dart';
import 'package:sikp/Model.dart';
import 'package:http/http.dart' show Client;

class apiservices {
  final String baseUrl = "https://192.168.18.36/db_kp";
  Client client = Client();

  Future<List<kp>> getKp() async {
    final response = await client.get(Uri.parse("$baseUrl/getKp.php"));
    if (response.statusCode == 200) {
      return kpFromJson(response.body);
    } else {
      return null;
    }
  }
  //---------------------------------------------------------------------------------------------
  Future<List<prakp>> getPraKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getPraKp.php"));
    if(response.statusCode == 200){
      return prakpFromJson(response.body);
    }else{
      return null;
    }
  }
//--------------------------------------------------------------------------------------------------
  Future<List<skkp>> getSkKp() async{
    final response = await client.get(Uri.parse("$baseUrl/getSkKp.php"));
    if(response.statusCode == 200){
      return skkpFromJson(response.body);
    }else{
      return null;
    }
  }
  //------------------------------------------------------------------------------------------------

}




