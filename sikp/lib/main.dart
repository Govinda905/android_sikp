import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sikp/Mahasiswa.dart';
import 'dart:io';

import 'Dosen.dart';
import 'Koor.dart';


void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIKP',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<void> _prosesLogin() async {
    final googleUser = await GoogleSignIn().signIn();
    if(googleUser != null && googleUser.email.contains("si.ukdw.ac.id")){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => Mahasiswa(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,)));
    } else if(googleUser != null && googleUser.email.contains("gmail.com")){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => Dosen(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,)));
    }else if(googleUser != null && googleUser.email.contains("students.ukdw.ac.id")) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => Koor(nama: googleUser.displayName, email: googleUser.email, foto: googleUser.photoUrl,)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget> [
                Image.asset('assets/KP_LOGO.jpg'),
                Text("KPUKDW 2021", style: TextStyle(fontSize: 18),),
                SizedBox(height: 20,),
                Text("Login User", style: TextStyle(fontSize: 18),),
                SizedBox(height: 20,),

                ElevatedButton(
                  child: Text("Sign  In With Google "),
                  onPressed: (){
                    _prosesLogin();
                  },
                ),

              ],
            )
        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

