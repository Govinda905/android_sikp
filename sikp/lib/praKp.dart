
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class TambahPraKp extends StatefulWidget {
  const TambahPraKp({Key key}) : super(key: key);

  @override
  _TambahPraKpState createState() => _TambahPraKpState();
}

class _TambahPraKpState extends State<TambahPraKp> {

  TextEditingController nimController = new TextEditingController();
  TextEditingController lemabagaController = new TextEditingController();
  TextEditingController alamatController = new TextEditingController();
  TextEditingController pimpinanController = new TextEditingController();
  TextEditingController no_telpController = new TextEditingController();
  TextEditingController faxController = new TextEditingController();
  TextEditingController spekController = new TextEditingController();
  TextEditingController toolsController = new TextEditingController();
  TextEditingController dokumenController = new TextEditingController();

  save() async {
    final response = await http.post(
        Uri.parse("http://192.168.18.36/db_kp/praKp.php"),
        body: {
          "nim": nimController.text,
          "lembaga": lemabagaController.text,
          "alamat": alamatController.text,
          "pimpinan": pimpinanController.text,
          "no_telp": no_telpController.text,
          "fax": faxController.text,
          "spek": spekController.text,
          "tools": toolsController.text,
          "dokumen": dokumenController.text,

        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan Pra KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh: 72180218",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: lemabagaController,
                    decoration: InputDecoration(
                        labelText: "lembaga :",
                        hintText: "nama lembaga",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: alamatController,
                    decoration: InputDecoration(
                        labelText: "alamat : ",
                        hintText: "nama alamat",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: pimpinanController,
                    decoration: InputDecoration(
                        labelText: "pimpinan :",
                        hintText: "nama pimpinan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: no_telpController,
                    decoration: InputDecoration(
                        labelText: "no_telp:",
                        hintText: " no tlp anda",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: faxController,
                    decoration: InputDecoration(
                        labelText: "fax :",
                        hintText: "isi dengan no fax",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: spekController,
                    decoration: InputDecoration(
                        labelText: "spek :",
                        hintText: "nama spek",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: toolsController,
                    decoration: InputDecoration(
                        labelText: "tools :",
                        hintText: "nama tools",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: dokumenController,
                    decoration: InputDecoration(
                        labelText: "dokumen :",
                        hintText: "nama dokumen",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Surat keterangan  KP"),
                              content: Text("yakin akan simpan ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () {
                                      save();
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: () {
                                  Navigator.pop(context);
                                }, child: Text("Tidak"))
                              ],
                            );
                          }
                      );
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
