
import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/apiservices.dart';
import 'package:sikp/kp.dart';
import 'package:sikp/menu_regiskp.dart';

class MenuKPMhs extends StatefulWidget {
  const MenuKPMhs({Key key}) : super(key: key);

  @override
  _MenuKPMhsState createState() => _MenuKPMhsState();
}

class _MenuKPMhsState extends State<MenuKPMhs> {

  List<kp> listkp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>TambahKp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: apiservices().getKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<kp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listkp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listkp[position].nim + " - " +
                            listkp[position].semester + " , " + listkp[position].tahun),
                        subtitle: Text(
                            "judul_kerja_praktik: " + listkp[position].judul_kerja_praktik + "\n" +
                                "status_kp : " + listkp[position].status_kp
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listkp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
