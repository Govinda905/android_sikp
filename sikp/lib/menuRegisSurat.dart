


import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/apiservices.dart';
import 'package:sikp/verifSuratKeterangan.dart';

class MenuRegisSurat extends StatefulWidget {
  const MenuRegisSurat({Key key}) : super(key: key);

  @override
  _MenuRegisSuratState createState() => _MenuRegisSuratState();
}

class _MenuRegisSuratState extends State<MenuRegisSurat> {

  List<skkp> listskkp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Surat Keterangan"),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: apiservices().getSkKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<skkp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listskkp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listskkp[position].nim + " - " +
                              listskkp[position].lembaga + " , " + listskkp[position].no_telp),
                          subtitle: Text(
                              "dokumen : " + listskkp[position].dokumen + "\n" +
                                  "status_skp : " + listskkp[position].status_skp
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>VerifSuratKeterangan()));
                    },
                  );
                },
                itemCount: listskkp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
