
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class VerifPraKp extends StatefulWidget {
  const VerifPraKp({Key key}) : super(key: key);

  @override
  _VerifPraKpState createState() => _VerifPraKpState();
}

class _VerifPraKpState extends State<VerifPraKp> {
  TextEditingController nimController = new TextEditingController(text: "72180218");

  saveTerima() async{
    final response = await http.post(Uri.parse("https://192.168.18.36/db_kp/verifPraKpTerima.php"),
        body: {
          "nim": nimController.text
        });
  }

  saveTolak() async{
    final response = await http.post(Uri.parse("https://192.168.18.36/db_kp/verifPraKpTolak.php"),
        body: {
          "nim": nimController.text
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Verif Pra Kp"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),
                  Text("Apakah anda setuju untuk menerima SK dengan nim dibawah ini ? "),
                  SizedBox(height: 15,),

                  SizedBox(height: 15,),
                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: <Widget>[
                      ElevatedButton(
                        onPressed: (){
                          saveTerima();
                          Navigator.pop(context);
                        },
                        child: Text("Terima"),
                      ),
                      Divider(
                        indent: 10,
                        endIndent: 10,
                      ),
                      ElevatedButton(
                        onPressed: (){
                          saveTolak();
                          Navigator.pop(context);
                        },
                        child: Text("Tolak"),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}


