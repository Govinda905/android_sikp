
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sikp/addSkMhs.dart';
import 'package:sikp/kp.dart';
import 'package:sikp/menuKP.dart';
import 'package:sikp/menuPaKp.dart';
import 'package:sikp/menuSuratKp.dart';
import 'package:sikp/menu_regiskp.dart';
import 'package:sikp/praKp.dart';

import 'main.dart';

class Mahasiswa extends StatefulWidget {
  final String nama, email, foto;

  Mahasiswa({this.nama, this.email, this.foto});
  @override
  _MahasiwaState createState() => _MahasiwaState();
}

class _MahasiwaState extends State<Mahasiswa> {
  Future<void> _keluar() async{
    await GoogleSignIn().signOut();

    Navigator.pushReplacement(context,
        MaterialPageRoute(
            builder: (context) => MyHomePage()
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("HALAMAN MAHASISWA"),
        backgroundColor: Colors.deepOrange[800],
      ),
      backgroundColor: Colors.deepOrange[100],

      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text("${widget.nama}", style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 17.0),),
              accountEmail: new Text(" ${widget.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(widget.foto),
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Keluar"),
              onTap: () => _keluar(),
            ),
          ],
        ),
      ),


      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuSuratKp()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file, size: 70.0,
                        color: Colors.green,),
                      Text("Pengajuan Surat KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuKPMhs()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file, size: 70.0,
                        color: Colors.deepOrangeAccent,),
                      Text("Pengajuan KP", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>MenuPraKp()));
                },
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.insert_drive_file, size: 70.0,
                        color: Colors.deepOrange,),
                      Text("Pengajuan Pra KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.date_range, size: 70.0, color: Colors.red,),
                      Text(
                          "Waktu Ujian", style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {},
                splashColor: Colors.green,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.location_pin, size: 70.0, color: Colors.green,),
                      Text("Lokasi KP",
                          style: new TextStyle(fontSize: 17.0))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
