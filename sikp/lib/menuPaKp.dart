

import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/apiservices.dart';
import 'package:sikp/praKp.dart';

class MenuPraKp extends StatefulWidget {
  const MenuPraKp({Key key}) : super(key: key);

  @override
  _MenuPraKpState createState() => _MenuPraKpState();
}

class _MenuPraKpState extends State<MenuPraKp> {
  List<prakp> listprakp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pra KP"),
        backgroundColor: Colors.blue[700],
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>TambahPraKp()));
              })
        ],
      ),

      body: FutureBuilder(
          future: apiservices().getPraKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<prakp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listprakp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return Card(
                    margin: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 5.0),
                    child: Container(
                      child: ListTile(
                        title: Text(listprakp[position].nim + " - " +
                            listprakp[position].lembaga + " , " + listprakp[position].lembaga),
                        subtitle: Text(
                            "no_telp : " + listprakp[position].no_telp + "\n" +
                            "dokumen : " + listprakp[position].dokumen
                        ),
                      ),
                    ),
                  );
                },
                itemCount: listprakp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),

    );
  }
}
