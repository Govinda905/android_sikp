
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class TambahKp extends StatefulWidget {
  const TambahKp({Key key}) : super(key: key);

  @override
  _TambahKpState createState() => _TambahKpState();
}

class _TambahKpState extends State<TambahKp> {

  TextEditingController semesterController = new TextEditingController();
  TextEditingController tahunController = new TextEditingController();
  TextEditingController judul_kerja_praktikController = new TextEditingController();
  TextEditingController toolsController = new TextEditingController();
  TextEditingController nimController = new TextEditingController();
  TextEditingController lemabagaController = new TextEditingController();
  TextEditingController alamatController = new TextEditingController();
  TextEditingController pimpinanController = new TextEditingController();
  TextEditingController no_telpController = new TextEditingController();
  TextEditingController faxController = new TextEditingController();
  TextEditingController spekController = new TextEditingController();
  TextEditingController dokumenController = new TextEditingController();

  save() async {
    final response = await http.post(
        Uri.parse("http://192.168.18.36/db_kp/Kp.php"),
        body: {
          "semester": semesterController.text,
          "tahun": tahunController.text,
          "judul_kerja_praktik": judul_kerja_praktikController.text,
          "tools": toolsController.text,
          "nim": nimController.text,
          "lembaga": lemabagaController.text,
          "alamat": alamatController.text,
          "pimpinan": pimpinanController.text,
          "no_telp": no_telpController.text,
          "fax": faxController.text,
          "spek": spekController.text,
          "dokumen": dokumenController.text,

        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pengajuan KP"),),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: semesterController,
                    decoration: InputDecoration(
                        labelText: "semester :",
                        hintText: "contoh: ganjil/genap",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: tahunController,
                    decoration: InputDecoration(
                        labelText: "tahun :",
                        hintText: "contoh: 2018",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: judul_kerja_praktikController,
                    decoration: InputDecoration(
                        labelText: "judul_kerja_praktik :",
                        hintText: "contoh: Pembuatan E-tiket",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: toolsController,
                    decoration: InputDecoration(
                        labelText: "tools :",
                        hintText: "nama tools",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: nimController,
                    decoration: InputDecoration(
                        labelText: "NIM :",
                        hintText: "contoh: 72180218",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: lemabagaController,
                    decoration: InputDecoration(
                        labelText: "lembaga :",
                        hintText: "nama lembaga",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: alamatController,
                    decoration: InputDecoration(
                        labelText: "alamat : ",
                        hintText: "nama alamat",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: pimpinanController,
                    decoration: InputDecoration(
                        labelText: "pimpinan :",
                        hintText: "nama pimpinan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: no_telpController,
                    decoration: InputDecoration(
                        labelText: "no_telp:",
                        hintText: " no tlp anda",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: faxController,
                    decoration: InputDecoration(
                        labelText: "fax :",
                        hintText: "isi dengan no fax",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: spekController,
                    decoration: InputDecoration(
                        labelText: "spek :",
                        hintText: "nama spek",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  TextFormField(
                    controller: dokumenController,
                    decoration: InputDecoration(
                        labelText: "dokumen :",
                        hintText: "nama dokumen",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(
                            20.0, 15.0, 20.0, 15.0)
                    ),
                  ),
                  SizedBox(height: 15,),

                  MaterialButton(
                    minWidth: MediaQuery
                        .of(context)
                        .size
                        .width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    color: Colors.cyan,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Surat KP"),
                              content: Text("Yakin untuk disimpan ?"),
                              actions: <Widget>[
                                TextButton(
                                    onPressed: () {
                                      save();
                                      Navigator.pop(context);
                                    },
                                    child: Text("Ya")
                                ),
                                TextButton(onPressed: () {
                                  Navigator.pop(context);
                                }, child: Text("Tidak"))
                              ],
                            );
                          }
                      );
                    },
                    child: Text(
                      "Simpan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),

                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );;
  }
}
