import 'dart:convert';

class kp {
  String id_kp;
  String semester;
  String tahun;
  String judul_kerja_praktik;
  String tools;
  String nim;
  String lembaga;
  String alamat;
  String pimpinan;
  String no_telp;
  String fax;
  String spek;
  String penguji;
  String dokumen;
  String status_kp;

  kp(
      {this.id_kp, this.semester, this.tahun, this.judul_kerja_praktik, this.tools, this.nim, this.lembaga, this.alamat, this.pimpinan, this.no_telp, this.fax, this.spek, this.penguji, this.dokumen, this.status_kp });

  factory kp.fromJson(Map<String, dynamic> map){
    return kp(
        id_kp: map["id_kp"],
        semester: map["semester"],
        tahun: map["tahun"],
        judul_kerja_praktik: map["judul_kerja_praktik"],
        tools: map["tools"],
        nim: map["nim"],
        lembaga: map["lembaga"],
        alamat: map["alamat"],
        pimpinan: map["pimpinan"],
        no_telp: map["no_telp"],
        fax: map["fax"],
        spek: map["spek"],
        penguji: map["penguji"],
        dokumen: map["dokumen"],
        status_kp: map["status_kp"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id_kp": id_kp,
      "semester": semester,
      "tahun": tahun,
      "judul_kerja_praktik": judul_kerja_praktik,
      "tools": tools,
      "nim": nim,
      "lembaga": lembaga,
      "alamat": alamat,
      "pimpinan": pimpinan,
      "no_telp": no_telp,
      "fax": fax,
      "spek": spek,
      "penguji": penguji,
      "dokumen": dokumen,
      "status_kp": status_kp
    };
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'kp(id_kp; $id_kp, semester: $semester, tahun: $tahun, judul_kerja_praktik: $tahun, tools: $tools, nim: $nim, lembaga: $lembaga, alamat: $alamat, pimpinan: $pimpinan, no_telp: $no_telp, fax: $fax, spek: $spek, penguji: $penguji, dokumen: $dokumen, status_kp: $status_kp)';
  }
}

List<kp> kpFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<kp>.from(data.map((item) => kp.fromJson(item)));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------

class prakp {
  String id_pkp;
  String nim;
  String lembaga;
  String alamat;
  String pimpinan;
  String no_telp;
  String fax;
  String spek;
  String tools;
  String dokumen;
  String status_pkp;

  prakp(
      {this.id_pkp, this.nim, this.lembaga, this.alamat, this.pimpinan, this.no_telp, this.fax, this.spek, this.tools, this.dokumen, this.status_pkp });

  factory prakp.fromJson(Map<String, dynamic> map){
    return prakp(
        id_pkp: map["id_kp"],
        nim: map["nim"],
        lembaga: map["lembaga"],
        alamat: map["alamat"],
        pimpinan: map["pimpinan"],
        no_telp: map["no_telp"],
        fax: map["fax"],
        spek: map["spek"],
        tools: map["tools"],
        dokumen: map["dokumen"],
        status_pkp: map["status_pkp"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id_kp": id_pkp,
      "nim": nim,
      "lembaga": lembaga,
      "alamat": alamat,
      "pimpinan": pimpinan,
      "no_telp": no_telp,
      "fax": fax,
      "spek": spek,
      "tools": tools,
      "dokumen": dokumen,
      "status_pkp": status_pkp
    };
  }

  @override
  String toString() {
    return 'pra_kp(id_pkp; $id_pkp, nim: $nim, lembaga: $lembaga, alamat: $alamat, pimpinan: $pimpinan, no_telp: $no_telp, fax: $fax, spek: $spek, tools: $tools, dokumen: $dokumen, status_pkp: $status_pkp)';
  }
}

List<prakp> prakpFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<prakp>.from(data.map((item) => prakp.fromJson(item)));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class skkp {
  String id_skp;
  String nim;
  String lembaga;
  String alamat;
  String pimpinan;
  String no_telp;
  String fax;
  String spek;
  String tools;
  String dokumen;
  String status_skp;

  skkp(
      {this.id_skp, this.nim, this.lembaga, this.alamat, this.pimpinan, this.no_telp, this.fax, this.spek, this.tools, this.dokumen, this.status_skp });

  factory skkp.fromJson(Map<String, dynamic> map){
    return skkp(
        id_skp: map["id_skp"],
        nim: map["nim"],
        lembaga: map["lembaga"],
        alamat: map["alamat"],
        pimpinan: map["pimpinan"],
        no_telp: map["no_telp"],
        fax: map["fax"],
        spek: map["spek"],
        tools: map["tools"],
        dokumen: map["dokumen"],
        status_skp: map["status_skp"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id_skp": id_skp,
      "nim": nim,
      "lembaga": lembaga,
      "alamat": alamat,
      "pimpinan": pimpinan,
      "no_telp": no_telp,
      "fax": fax,
      "spek": spek,
      "tools": tools,
      "dokumen": dokumen,
      "status_skp": status_skp
    };
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'surat_kp(id_skp; $id_skp, nim: $nim, lembaga: $lembaga, alamat: $alamat, pimpinan: '
        '$pimpinan, no_telp: $no_telp, fax: $fax, spek: $spek, tools: $tools, dokumen: $dokumen, status_skp: $status_skp)';
  }
}

List<skkp> skkpFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<skkp>.from(data.map((item) => skkp.fromJson(item)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

