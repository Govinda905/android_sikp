


import 'package:flutter/material.dart';
import 'package:sikp/Model.dart';
import 'package:sikp/apiservices.dart';
import 'package:sikp/verifPraKp.dart';

class MenuRegisPrakp extends StatefulWidget {
  const MenuRegisPrakp({Key key}) : super(key: key);

  @override
  _MenuRegisPrakpState createState() => _MenuRegisPrakpState();
}

class _MenuRegisPrakpState extends State<MenuRegisPrakp> {

  List<prakp> listprakp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(" Verif PRA KP "),
        backgroundColor: Colors.blue[700],
      ),

      body: FutureBuilder(
          future: apiservices().getPraKp(),
          builder: (BuildContext context,
              AsyncSnapshot<List<prakp>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text("Something wrong with message: ${snapshot.error
                    .toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              listprakp = snapshot.data;

              return ListView.builder(
                itemBuilder: (context, position) {
                  return InkWell(
                    child: Card(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 5.0),
                      child: Container(
                        child: ListTile(
                          title: Text(listprakp[position].nim + " - " +
                              listprakp[position].lembaga + " , " + listprakp[position].no_telp),
                          subtitle: Text(
                              "dokumen : " + listprakp[position].dokumen + "\n" +
                              "status : " + listprakp[position].status_pkp
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>VerifPraKp()));
                    },
                  );
                },
                itemCount: listprakp.length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
    );
  }
}
